// Importa o módulo express
const express = require('express');
// Cria uma instância do express
const app = express();
// Define a porta em que o servidor irá escutar
const PORT = 3001;

// Configuração para permitir o uso de arquivos estáticos (por exemplo, CSS)
app.use(express.static('public'));

// Configuração para usar o mecanismo de visualização EJS
app.set('view engine', 'ejs');

// Rota para a página inicial
app.get('/', (req, res) => {
    res.render('index');
});

// Rota para a página de objetivos
app.get('/objetivos', (req, res) => {
    res.render('objetivos');
});

// Rota para a página de desenvolvimento
app.get('/desenvolvimento', (req, res) => {
    res.render('desenvolvimento');
});

// Rota para a página de conclusão
app.get('/conclusao', (req, res) => {
    res.render('conclusao');
});

// Inicia o servidor na porta especificada
app.listen(PORT, () => {
    console.log(`Servidor rodando em http://localhost:${PORT}`);
});
