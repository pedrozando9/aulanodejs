const readline = require('readline-sync');

console.log("");

const a = parseFloat(readline.question('digite o valor de "a": '));
const b = parseFloat(readline.question('digite o valor de "b": '));
const c = parseFloat(readline.question('digite o valor de "c": '));

function teste(a, b ,c){
    if (Math.abs(a-b) < c && c < (a+b))return true 
    else return false;
}

console.log("----------------------------------------------------------------------------------------------------");

if(a != 0 && b !=0 && c !=0) {
    if(teste(a, b, c) &&
        teste(a, c, b) &&
        teste(b, c, a)) console.log("É Triangulo!");
    else console.log("Não é triangulo!");
} else console.log("Não pode haver valor nulo!");

console.log("");
